# Verifiable Research

How to make and implement an open and transparent workflow for conducting research in a way that is verifiable by yourself and everyone else (based on the adventures of [Corina Logan](http://corinalogan.com) at [#BulliedIntoBadScience](http://bulliedintobadscience.org) and [PCI Ecology](https://ecology.peercommunityin.org/PCIEcology))

## Table of Contents
 - [GitHub or GitLab?](#github-or-gitlab)
 - [Tip](#tip-on-how-to-structure-your-repositorys-main-page) on how to structure your repository's main page
 - [How to easily edit GitLab pages and GitHub files](#how-to-easily-edit-gitlab-pages-and-gitHub-files)

### GitHub or GitLab?

Vicky Steeve's has a cool [blog post](https://vickysteeves.com/blog/gitlab-repro/) on how GitLab can help in research reproducibility, and she weighs in on [why she prefers GitLab to GitHub](https://twitter.com/LoganCorina/status/1134352787406512128)

### Tip on how to structure your repository's main page

Add a folder “Files” (push it through GitHub Desktop or Git) and put all docs there so your home page doesn’t get super long. See an example at my [GitHub grackle repository](https://github.com/corinalogan/grackles). If you click on the Files folder and open all of the other folders in there, you will see that there are loads of files! Can you imagine having to scroll through all of those files jus to get to the Table of Contents at my repo's main page?

### How to easily edit GitLab pages and GitHub files

**GitLab pages**
*  Wiki Pages: click on the wiki you want to edit, click the Edit button at the top of the page near the middle, make changes to the document (it is written in markdown language - here is a [markdown cheatsheet](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)). To save the changes, scroll to the bottom of the page and write in the *Commit message* box what you changed, then click the *Save changes* button.
*  Project > README.md: click the README.md file name, click the Edit button near the top right, follow the rest of the instructions as above for Wiki Pages.
*  To make/edit a To Do list, click Issues > Board. Click the + sign at the top right of the board to make a new issue (you can set a deadline, assign it to someone, add another label to it, etc.). To edit an item (card), click the card text, then click the pencil button to the write of the title text, make changes, then click Save changes.
*  To create new folders and move files from folder to folder, you can [connect your computer](https://itnext.io/how-to-use-github-desktop-with-gitlab-cd4d2de3d104) to the GitLab repository through [GitHub Desktop](https://desktop.github.com). Then create the folders you want on your computer, move whatever files you want there, open GitHub Desktop and you will see them in your repository as new changes, make the commit and then push them to your GitHub/Lab repository.

**GitHub files**
1) Go to the Rmd (or md) file you want to edit
2) Click on the pencil icon near the top right of the document (hover over says “edit this file”). This brings you to an editable version of the document. Write and delete whatever you want here as you would write in for example Word. It is an RMarkdown file so if you want to format things, you can find which characters do what formatting in a great cheatsheet: https://www.rstudio.com/wp-content/uploads/2016/03/rmarkdown-cheatsheet-2.0.pdf and reference guide: https://www.rstudio.com/wp-content/uploads/2015/03/rmarkdown-reference.pdf. For the text in the document, the language is markdown, and the cool part about Rmarkdown files is that you can throw in your R code as well. You probably won’t need to do any formatting because there is already formatting in place, but just in case you do, these are handy links.
3) When you are ready to save a version (you can save often, it’s totally ok to save multiple times part way through making changes), scroll to the bottom of the page to the section “Commit changes”. In the text box that has light gray text “Update X.Rmd”, write a quick note about what you did (e.g., changed a prediction, added details to Planned Sample, etc.), keep the default button ticked to “Commit directly to the master branch”, and then click the green button “Commit changes”.

That’s it! If you click on the link to the Rmd file again and click on the History button near the top right corner of the screen, you (and everyone else) can see the changes you just made :) So it is a publicly viewable scientific writing process :) 

